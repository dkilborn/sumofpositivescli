package com.galvanize;

public class SumOfPositivesCLI {
    public static void main(String[] args) {
        int totalPositive =0;
        for (int i = 0; i < args.length ; i++) {
            if(Integer.parseInt(args[i])>0){
                totalPositive+= Integer.parseInt(args[i]);
            }
        }
        System.out.println(totalPositive);
    }
    }
